prometheus-node-exporter-collectors (0.0~git20241119.a2b43e1-1) unstable; urgency=medium

  * New upstream snapshot to support nvme-cli v2.11+ (Closes: #1087766)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Tue, 19 Nov 2024 18:18:56 +0000

prometheus-node-exporter-collectors (0.0~git20240509.18fcb1c-1) unstable; urgency=medium

  * New upstream snapshot
  * Normalize all collector .services to use /bin/sh
  * Add postrm to remove .prom files upon purge
  * Drop unnecessary daemon | systemd-sysv dependency
  * Normalize .service ConditionFileIsExecutable
  * Execute collector-sponge pipe command with '-o pipefail'
  * Bump Standards-Version to 4.7.0 (no changes)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Wed, 12 Jun 2024 11:53:06 +0000

prometheus-node-exporter-collectors (0.0~git20240427.1cb6223-1) unstable; urgency=medium

  * New upstream snapshot (Closes: #1065367)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Thu, 02 May 2024 12:54:35 +0000

prometheus-node-exporter-collectors (0.0~git20231018.f5c56e7-2) unstable; urgency=medium

  * Demote prometheus-node-exporter Depends to Enhances (Closes: #1064415)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Mon, 18 Mar 2024 21:11:50 +0000

prometheus-node-exporter-collectors (0.0~git20231018.f5c56e7-1) unstable; urgency=medium

  * Team upload.
  * New upstream snapshot, fixing the apt_package_cache_timestamp_seconds
    metric (related to #1028212)

 -- Antoine Beaupré <anarcat@debian.org>  Fri, 20 Oct 2023 09:55:25 -0400

prometheus-node-exporter-collectors (0.0~git20231016.66010f0-1) unstable; urgency=medium

  * Team upload.
  * New upstream snapshot (Closes: #1028212)

 -- Antoine Beaupré <anarcat@debian.org>  Mon, 16 Oct 2023 15:27:25 -0400

prometheus-node-exporter-collectors (0.0~git20230625.ad4d9e8-1) unstable; urgency=medium

  * New upstream snapshot (fix nvme_metrics.py device label)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Sun, 25 Jun 2023 14:14:31 +0000

prometheus-node-exporter-collectors (0.0~git20230616.14e1f94-2) unstable; urgency=medium

  * Add missing python3-prometheus-client dependency
  * Switch to Python-based NVMe collector

 -- Daniel Swarbrick <dswarbrick@debian.org>  Sun, 25 Jun 2023 11:21:09 +0000

prometheus-node-exporter-collectors (0.0~git20230616.14e1f94-1) unstable; urgency=medium

  * New upstream snapshot
  * Set LC_ALL=C in ipmitool-sensor service (Closes: #1031858)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Mon, 19 Jun 2023 23:52:45 +0000

prometheus-node-exporter-collectors (0.0~git20230203.6f710f8-1) unstable; urgency=medium

  * New upstream snapshot (Closes: #1030058)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Fri, 03 Feb 2023 04:57:45 +0000

prometheus-node-exporter-collectors (0.0~git20230110.843e550-1) unstable; urgency=medium

  * New upstream snapshot (Closes: #992409)
  * Bump Standards-Version to 4.6.2 (no changes)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Tue, 10 Jan 2023 00:35:52 +0000

prometheus-node-exporter-collectors (0.0~git20221011.8f6be63-1) unstable; urgency=medium

  * New upstream snapshot
  * Update gbp configuration following Go Team new workflow
  * Adjust service and dependencies for replacement apt_info.py script
  * debian/control: update Recommends packages
  * Bump Standards-Version to 4.6.1 (no changes)
  * Add debian/watch

 -- Daniel Swarbrick <dswarbrick@debian.org>  Tue, 11 Oct 2022 16:24:12 +0000

prometheus-node-exporter-collectors (0+git20211024.8eeeffb-1) unstable; urgency=medium

  [ Aloïs Micard ]
  * Update debian/gitlab-ci.yml (using pkg-go-tools/ci-config)

  [ Daniel Swarbrick ]
  * New upstream snapshot (Closes: #993676)
  * Bump Standards-Version to 4.6.0 (no changes)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Sun, 05 Dec 2021 18:47:32 +0000

prometheus-node-exporter-collectors (0+git20210115.7d89f19-1) unstable; urgency=medium

  * Team upload.
  * New upstream snapshot.
  * Bump Standards-Version to 4.5.1
  * Add systemd timer and service for nvme_metrics.sh

 -- Benjamin Drung <benjamin.drung@cloud.ionos.com>  Fri, 15 Jan 2021 16:45:09 +0100

prometheus-node-exporter-collectors (0+git20201003.8db38d1-1) unstable; urgency=medium

  [ Benjamin Drung ]
  * Team upload.
  * New upstream snapshot.
  * Start prometheus-node-exporter-mellanox-hca-temp after rdma-hw.target and
    network-pre.target (for rdma-hw to settle).
  * Start prometheus-node-exporter-ipmitool-sensor after systemd-modules-load.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository-Browse.

 -- Benjamin Drung <benjamin.drung@cloud.ionos.com>  Tue, 10 Nov 2020 18:02:13 +0100

prometheus-node-exporter-collectors (0+git20200718.57d05ce-1) unstable; urgency=medium

  * New upstream snapshot.
  * Bump debhelper-compat version to 13.
  * Remove inappropriate autopkgtest-pkg-go testsuite (Closes: #953893).
  * Update maintainer email address.

 -- Daniel Swarbrick <daniel.swarbrick@cloud.ionos.com>  Mon, 03 Aug 2020 12:27:45 +0200

prometheus-node-exporter-collectors (0+git20200110.fc91c86-1) unstable; urgency=medium

  * Initial release.
  * Import textfile collector scripts from prometheus-node-exporter package.

 -- Daniel Swarbrick <daniel.swarbrick@cloud.ionos.com>  Mon, 17 Feb 2020 14:56:18 +0100
